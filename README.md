Description:
	this project fetch Youtube latest search data in every 6 hour and update it into DB.
	Using the Dashboard you can see the fetched data.
	
Tech Stack:
	NodeJs, Express, MySql, BackboneJS, JQuery and etc.

Setup:

	- NodeJS server setup:
		- First of all Make sure the latest version of the following Modules and apllications are already installed in your system.
			- NPM, NoeJS, Mysql
		- Clone the repository.
		- Run the the command in root directory of repository to install node modules.
			'npm install'
		- Add the MySQL DB credentials in './Models/DBConnection.js' file.
		- By default server runs on 9555 port, you can change it in 'server.js' file.
		- Default Youtube search parameter is 'ipl', if required you can change it in './Models/DataFetchCronJob.js' file.

	- MySql Setup:
		
		- Run the query to create a table:
			CREATE TABLE `youtube_videos` (
			  `ID` int(11) NOT NULL AUTO_INCREMENT,
			  `VIDEO_ID` varchar(255) NOT NULL,
			  `TITLE` varchar(255) NOT NULL,
			  `DESCRIPTION` varchar(255) NOT NULL,
			  `PUBLISHED_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			  `THUMBNAILS` json NOT NULL,
			  PRIMARY KEY (`VIDEO_ID`),
			  KEY `ID` (`ID`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1';
	
	- Run Server:
		
		- Run the command to start node server:
			'node server.js'
		- Ping the URL to see the stored data:
			'http://localhost:9555'
