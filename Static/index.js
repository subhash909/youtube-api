var DesktopPage =  Backbone.View.extend({
    
    events: {
        "click #prev_button": "prev_button_clicked",
        "click #next_button": "next_button_clicked",
    },

    initialize: function(){
        this.page_index = 0;
    },

    // render the main template
    render: function(){
        $(this.el).html(this.main_template());
        this.fetch_video_data();
        return this;
    },

    // fetch video details from server
    fetch_video_data: function(){
        var that = this;
        $.ajax({
            url: '/getvist?p_ind='+this.page_index,
            type: 'GET',
            dataType: 'json',
            success: function(result){
                that.render_video_details(result);
            },
            error: function(err){
                console.log("Error occurred in fetch_video_data: ", err);
            }
        });
    },

    // render video details in page
    render_video_details: function(results){
        try{
            var rows_html = this.table_rows_template({results: results});
            $(this.el).find('#table_body').html(rows_html);
            $(this.el).find('#page_index').text(this.page_index+1);
            $(this.el).find('#table_id').DataTable({
                "bPaginate": false,
                "info": false,
                "order": [[ 2, "desc" ]],
                "scrollY": '470px',
                "scrollCollapse": true,
            });
        }catch(err){
            console.log("Error occurred in render_video_details: ", err);
        }
    },

    // previous page clicked
    prev_button_clicked: function(event){
        if(this.page_index>0){
            $(this.el).html(this.main_template());
            this.page_index -= 1;
            this.fetch_video_data();
        }
    },

    // next page clicked
    next_button_clicked: function(event){
        console.log("next_button clicked");
        $(this.el).html(this.main_template());
        this.page_index += 1;
        this.fetch_video_data();
    },

    // main template
    main_template: _.template(
        '<div style="margin: 10px;">'+
            '<table id="table_id" class="display">'+
                '<thead>'+
                    '<tr>'+
                        '<th>TITLE</th>'+
                        '<th>DESCRIPTION</th>'+
                        '<th>PUBLISHED_DATE</th>'+
                    '</tr>'+
                '</thead>'+
                '<tbody id="table_body">'+
                '</tbody>'+
            '</table>'+
            '<br/><div style="text-align: center;">'+
                '<div class="btn btn-primary" id="prev_button">Previous</div>&nbsp;&nbsp;'+
                '<span id="page_index"> 1 </span>&nbsp;&nbsp;'+
                '<div class="btn btn-primary" id="next_button">Next</div>'+
            '</div>'+
        '</div>'
    ),

    // table row template
    table_rows_template: _.template(
        '<% for(var tmp_ind in results){  var tmp_obj = results[tmp_ind]; %>'+
            '<tr>'+
                '<td><%= tmp_obj.TITLE %></td>'+
                '<td><%= tmp_obj.DESCRIPTION %></td>'+
                '<td><%= new Date(tmp_obj.PUBLISHED_DATE) %></td>'+
            '</tr>'+
        '<% } %>'
    )

});

new DesktopPage({el: $("#body")}).render();