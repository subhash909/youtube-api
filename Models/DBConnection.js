var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : '',
  password : '',
  database : ''
});

connection.connect();

var get_db_connection = function(){
    return connection;
}

module.exports.get_db_connection = get_db_connection;