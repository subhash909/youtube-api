var request = require('request');
var db_connection = require('./DBConnection');

// Youtube serach parameter
var search_param = "ipl";

//Youtube access API key
var api_key = "AIzaSyDBjOUfeSTM8KEUYl1VcKrE_EkJYsTLE5M";

// Youtube API call for data
var fetch_api_data = function(nextPageToken){
    var timestamp = new Date(new Date().getTime() - (6*60*60*1000)).toISOString();
    console.log("fetch_api_data called: ", timestamp, nextPageToken || "-");
    
    var fetch_url = `https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&key=${api_key}&q=${search_param}&order=date&publishedAfter=${timestamp}&maxResults=50`;
    nextPageToken && (fetch_url += `&pageToken=${nextPageToken}`);
    request(fetch_url, (err, response, result)=>{
        if(!err && response.statusCode == 200 ){
            result = JSON.parse(result);
            store_data(result);
        }else{
            console.log("Error occurred in Youtube API call", err || response.statusCode);
        }
    });
}

// Insert data into DB
var store_data = function(result){
    try{
        if(result && result.items){
            var nextPageToken = result.nextPageToken || "";
            var sql_query = generate_insert_query(result.items);
            var connection = db_connection.get_db_connection();
            sql_query && connection.query(sql_query, (err, result) => {
                if(err) console.log(err);
                else{
                    nextPageToken && fetch_api_data(nextPageToken);
                }
            });
        }
    }catch(err){
        console.log("Error occurred in store_data: ", err);
    }
}

// Generate SQL insert query
var generate_insert_query = function(rows){
    console.log("generate_insert_query called");
    var tmp_query = "";
    try{
        var tmp_sub_query = [];
        for(tmp_ind in rows){
            var tmp_row_obj = rows[tmp_ind];
            tmp_sub_query.push(`(${JSON.stringify(tmp_row_obj.id.videoId)},${JSON.stringify(tmp_row_obj.snippet.title)},${JSON.stringify(tmp_row_obj.snippet.description)},${JSON.stringify(tmp_row_obj.snippet.publishedAt)},'${JSON.stringify(tmp_row_obj.snippet.thumbnails)}')`);
        }
        if(tmp_sub_query.length>0){
            tmp_query = "INSERT IGNORE youtube_videos(VIDEO_ID, TITLE, DESCRIPTION, PUBLISHED_DATE, THUMBNAILS) VALUES";
            tmp_query += tmp_sub_query.toString();
        }
    }catch(err){
        console.log("generate_insert_query: ", err);
    }
    return tmp_query;
}

// fetch video data from youtube in interval of 6 hour
setInterval(fetch_api_data, 6*60*60*1000);
fetch_api_data();