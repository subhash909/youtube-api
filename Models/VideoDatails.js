var db_connection = require('./DBConnection');

var get_video_details = function(req, res){
    var data = req.query || {};
    var page_index = data.p_ind || 0;
    fetch_video_datials(res, page_index);
}

var fetch_video_datials = function(res, page_index){
    var connection = db_connection.get_db_connection();
    var interval = 20;
    var start_no = interval * page_index;
    var fetch_query = "SELECT * FROM `youtube_videos` ORDER BY `PUBLISHED_DATE` DESC LIMIT "+start_no+","+interval;
    connection.query(fetch_query, (err, result) => {
        if(err){
            console.log("Error occurred in running fetch query: ", err);
            res.status(500);
            res.end();
        }
        else{
            res.send(result);
        }
    });
}

var export_obj = {};
export_obj.get_video_details = get_video_details;

module.exports = export_obj;