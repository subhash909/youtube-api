var http = require("http");
var express = require("express");
var path = require("path");

var data_fetch_cron_job = require('./Models/DataFetchCronJob');
var video_details = require('./Models/VideoDatails');
var bodyParser = require('body-parser');

// Express intialisation
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routing
app.use('/static', express.static(path.join(__dirname, 'Static/')));
app.route('/')
.get((req, res)=>{
    res.sendFile(path.join(__dirname + '/Static/index.html'));
});
app.route('/getvist')
.get(video_details.get_video_details);

// Server start
http.createServer(app).listen(9555);